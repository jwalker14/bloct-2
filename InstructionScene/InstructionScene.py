font = pygame.font.SysFont(None, 14) 

class InstructionScene():
    '''InstructionScene handles drawing and events for the pop up message dialog
    that displays the game instructions to the user via a button in BoardScene'''
    def __init__(self):
        okbtn = pygame.image.load ('okbutton.bmp').convert()
        #startingpoint = pygame.image.load ('startingpoint.jpg')
        #move1 = pygame.image.load ('move1.jpg')
        #move2 = pygame.image.load ('move2.jpg')
        #final = pygame.image.load ('final.jpg')

        text = font.render ('''Solve the puzzle by getting the red block from it's starting point in the lower right hand corner to it's end point in the upper left hand corner.
/Move the green and yellow blocks, side-to-side or up and down to clear a path for red.
/The blue blocks are stationary obstacles.
/Once the red block has reached it's destination, the game will proceed to the next level, each puzzle harder than the last.
/A counter in the corner will indicate the number of moves you have performed. The fewer the moves, the higher the score.''', True, black)                           
        textRect = text.get_rect()
        textRect.centerx = background.get_rect().centerx
        background.blit(text, textRect)

        self.ok = Button (okbtn)
        self.background = pygame.Surface ((windowwidth, windowheight)).convert()
        self.background.fill (lightgray)

        self.layout()

    def draw (self, screen):
        screen.blit (self.background, (0, 0))

    def handle_event (self, event):
        global scene
        if event.type == pygame.MOUSEBUTTONUP:
            if self.ok.rect.collidepoint(pygame.mouse.get_pos()):
                scene = scenes ["Board"]

    def layout (self):
        x = windowwidth 
        y = windowheight
        self.background.blit (text, textRect)
        self.ok.rect.x = windowwidth
        self.ok.rect.y = 100
        self.background.blit (self.ok.image, (self.ok.rect.x, self.ok.rect.y))

