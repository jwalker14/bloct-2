import pygame, csv
from pygame.locals import *
pygame.mixer.init()

white = (255, 255, 255)
lightgray = (225, 225, 225)
darkblue = (131, 147, 202)
black = (0, 0 , 0)
windowwidth = 800
windowheight = 600
scenes = {}
scene = None

class Block(pygame.sprite.Sprite):
    '''Represents a tile sprite'''
    def __init__(self, image, static, x, y, player):
        pygame.sprite.Sprite.__init__(self)
        self.image = image
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y                  
        self.static = static 
        self.player = player

class Board():
    '''Contains the individual tile sprites, sets their initial
    positions and handles their movements'''
    def __init__(self, layout, level, count):
        self.level = level
        self.layout = self.copy_layout(layout)
        self.count = count
        self.boardwidth = len(self.layout[0]) # Number of tiles
        self.boardheight = len(self.layout)
        self.tilesize = 50
        self.xmargin = (windowwidth - (self.tilesize * self.boardwidth)) / 2
        self.ymargin = ((windowheight - (self.tilesize * self.boardheight)) / 2) - 26
        self.rows = []
        self.cols = []
        
        self.allsprites = pygame.sprite.RenderPlain()
        self.selected = pygame.sprite.GroupSingle()
        self.player = pygame.sprite.GroupSingle()

        self.setup_board(self.layout)

    def copy_layout(self, layout):
        copy = []
        for row in range(len(layout)):
            sub = []
            for col in range(len(layout[0])):
                sub.append(layout[row][col])
            copy.append(sub)
        return copy
        #print(layout)

    def solved(self):
        if self.player.sprite.rect.x == self.finishcoord[0] and self.player.sprite.rect.y == self.finishcoord[1]:
            return True
        else:
            return False
                
    def setup_board(self, layout):
        '''Instantiates the tile sprites and places them based on the layout'''
        redtile = pygame.image.load("images/redtile.jpg").convert()
        barrier = pygame.image.load("images/barrier.jpg").convert()
        greentile = pygame.image.load("images/greentile.jpg").convert()
        horizontalrect = pygame.image.load("images/horizontalrect.jpg").convert()
        verticalrect = pygame.image.load("images/verticalrect.jpg").convert()
        

        for row in range(self.boardheight):
            for col in range(self.boardwidth):
                x = self.to_x(col)
                y = self.to_y(row)
                if self.layout[row][col] == 'S':
                    self.allsprites.add(Block(greentile, False, x, y, False))
                elif self.layout[row][col] == 'B':
                    self.allsprites.add(Block(barrier, True, x, y, False))
                elif self.layout[row][col] == 'H':
                    self.allsprites.add(Block(horizontalrect, False, x, y, False))
                elif self.layout[row][col] == 'V':
                    self.allsprites.add(Block(verticalrect, False, x, y, False))
                elif self.layout[row][col] == 'P':
                    s = Block(redtile, False, x, y, True)
                    self.allsprites.add(s)
                    self.player.add(s)
                elif self.layout[row][col] == 'F':
                    self.finishcoord = (x, y)
                self.rows.append(y)
                self.cols.append(x)    
                

    def to_x(self, col):
        '''Takes a column and outputs the x coordinate of the column'''
        return self.xmargin + col * self.tilesize

    def to_y(self, row):
        '''Takes a row and outputs the y coordinate of the row'''
        return self.ymargin + row * self.tilesize

    def to_col(self, x):
        '''Takes an x coordinate and outputs the column that contains the coordinate'''
        return (x - self.xmargin) / self.tilesize

    def to_row(self, y):
        '''Takes a y coordinate and outputs the row that contains the coordinate'''
        return (y - self.ymargin) / self.tilesize

    def is_square(self, sprite):
        '''Determines whether or not the sprite is a square or a rectangle'''
        if sprite.rect.w == sprite.rect.h:
            return True
        else:
            return False
            
                

    def select_tile(self):
        '''Checks to see if a mouse click occurs within a sprite,
        if so, selects the sprite'''
        self.mousepoint = pygame.mouse.get_pos()
        for sprite in self.allsprites:
            if sprite.rect.collidepoint(self.mousepoint) and not sprite.static:
                self.selected.add(sprite)
                self.previouscoord = (sprite.rect.x, sprite.rect.y)
                # Update the list to reflect the fact that because a
                # sprite was selected the cell is now empty
                row = self.to_row(sprite.rect.y)
                col = self.to_col(sprite.rect.x)
                if (sprite.rect.x, sprite.rect.y) == self.finishcoord:
                    self.layout[row][col] = 'F'
                else:    
                    self.layout[row][col] = 'E'
                # Dealing with a Rectangle, adjacent tile need
                # to be recorded as well
                if sprite.rect.w > sprite.rect.h: # Horizontal Rectangle
                    if (sprite.rect.x + self.tilesize, sprite.rect.y) == self.finishcoord:
                        self.layout[row][col + 1] = 'F'
                    else:
                        self.layout[row][col + 1] = 'E'
                elif sprite.rect.w < sprite.rect.h: # Vertical Rectangle
                    if (sprite.rect.x, sprite.rect.y + self.tilesize) == self.finishcoord:
                        self.layout[row + 1][col] = 'F'
                    else:
                        self.layout[row + 1][col] = 'E'

    def move_tile(self):
        '''Moves the selected tile sprite within the generated bounds'''
        # Find the boundaries for legal moves
        if self.horizontal_valid():
            leftbound = self.find_left_bound()
            rightbound = self.find_right_bound()
        else:
            leftbound = self.selected.sprite.rect.x
            rightbound = self.selected.sprite.rect.x

        if self.vertical_valid():
            upperbound = self.find_upper_bound()
            lowerbound = self.find_lower_bound()
        else:
            upperbound = self.selected.sprite.rect.y
            lowerbound = self.selected.sprite.rect.y
    
        lastmousepoint = self.mousepoint
        self.mousepoint = pygame.mouse.get_pos()
        # Since a tile can only be moved along one axis at a time,
        # we find out which direction the user wants to move the
        # tile by determining which value is greater: the change in x
        # or the change in y
        changex = self.mousepoint[0] - lastmousepoint[0]
        changey = self.mousepoint[1] - lastmousepoint[1]
        if abs(changex) > abs(changey):
            self.selected.sprite.rect.x += changex
        elif abs(changey) > abs(changex):
            self.selected.sprite.rect.y += changey

        # Stay within the boundaries
        if self.selected.sprite.rect.x < leftbound:
            self.selected.sprite.rect.x = leftbound
        elif self.selected.sprite.rect.x > rightbound:
            self.selected.sprite.rect.x = rightbound
        elif self.selected.sprite.rect.y < upperbound:
            self.selected.sprite.rect.y = upperbound
        elif self.selected.sprite.rect.y > lowerbound:
            self.selected.sprite.rect.y = lowerbound

    def drop_tile(self):
        '''Drops the selected tile sprite into the correct cell'''
        self.find_projected_cell()
        row = self.to_row(self.selected.sprite.rect.y)
        col = self.to_col(self.selected.sprite.rect.x)
        if self.is_square(self.selected.sprite): # Dealing with a Square
            if self.selected.sprite.player:
                self.layout[row][col] = 'P'
            else:
                self.layout[row][col] = 'S'
            
        else: # Dealing with a Rectangle
            if self.selected.sprite.rect.w > self.selected.sprite.rect.h: # Horizontal Rectangle
                self.layout[row][col] = 'H'
                self.layout[row][col + 1] = 'N'
            else: # Vertical Rectangle
                self.layout[row][col] = 'V'
                self.layout[row + 1][col] = 'N'
        newcoord = (self.selected.sprite.rect.x, self.selected.sprite.rect.y)       
        self.selected.empty()

        if not self.previouscoord == newcoord:
            self.count -= 1
            drop= pygame.mixer.Sound("drop-block.wav")
            drop.play()

        
    

        

        #debugging
        #print(self.layout)
        #print(self.level)

    

    
    def find_projected_cell(self):
        '''If a tile is released without being completely within a cell,
        moves the tile to the cell it is most overlapping'''
        spritex = self.selected.sprite.rect.x
        spritey = self.selected.sprite.rect.y
        cellrow = self.to_row(spritey)
        cellcol = self.to_col(spritex)
        if spritex in self.cols and spritey in self.rows:
            return
        if spritex in self.cols: # Tile is in vertical limbo
            if spritey - self.to_y(cellrow) > self.tilesize / 2:
                self.selected.sprite.rect.y = self.to_y(cellrow + 1)
            else:
                self.selected.sprite.rect.y = self.to_y(cellrow)
        else: # Tile is in horizontal limbo
            if spritex - self.to_x(cellcol) > self.tilesize / 2:
                self.selected.sprite.rect.x = self.to_x(cellcol + 1)
            else:
                self.selected.sprite.rect.x = self.to_x(cellcol)

    def horizontal_valid(self):
        '''Determines whether or not a sprite can slide horizontally'''
        if self.selected.sprite.rect.y in self.rows:
            return True
        else:
            return False

    def vertical_valid(self):
        '''Determines whether or not a sprite can slide vertically'''
        if self.selected.sprite.rect.x in self.cols:
            return True
        else:
            return False

    def empty(self, cell):
        return cell == 'E' or cell == 'F'

    def find_left_bound(self):
        '''Returns the left bound for the selected tile sprite'''
        row = self.to_row(self.selected.sprite.rect.y)
        col = self.to_col(self.selected.sprite.rect.x)
        left = col - 1
        # Start by checking if the selected sprite is a vertical rectangle
        # because they have special move conditions traveling horizontally
        if self.selected.sprite.rect.w < self.selected.sprite.rect.h:
            while left >= 0:
                # Start with the column to the left, if the cell is not empty
                # that is the the boundary
                if not self.empty(self.layout[row][left]):
                    return self.xmargin + left * self.tilesize + self.tilesize
                # Have to check one row down as well because the rectangle occupies
                # two spaces in the grid
                if not self.empty(self.layout[row + 1][left]):
                    return self.xmargin + left * self.tilesize + self.tilesize
                left -= 1
            # If the loop terminates without finding anything the
            # boundary is the left margin
            return self.xmargin
        else:
            # For a square or horizontal rectangle
            while left >= 0:
                if not self.empty(self.layout[row][left]):
                    return self.xmargin + left * self.tilesize + self.tilesize
                left -= 1
            return self.xmargin

    def find_right_bound(self):
        '''Returns the right bound for the selected tile sprite'''
        # Same idea as find_left_bound()
        row = self.to_row(self.selected.sprite.rect.y)
        col = self.to_col(self.selected.sprite.rect.x)
        right = col + 1
        if self.selected.sprite.rect.w < self.selected.sprite.rect.h:
            while right < self.boardwidth:
                # Since the rectangle takes up two cells, both
                # have to be checked and the first obstruction
                # found is returned
                if not  self.empty(self.layout[row][right]):
                    return self.xmargin + right * self.tilesize - self.tilesize
                if not  self.empty(self.layout[row + 1][right]):
                    return self.xmargin + right * self.tilesize - self.tilesize
                right += 1
            return self.xmargin + self.boardwidth * self.tilesize - self.tilesize
        else:
            while right < self.boardwidth:
                if not self.empty(self.layout[row][right]):
                    return self.xmargin + right * self.tilesize - self.selected.sprite.rect.w
                right += 1
            return self.xmargin + self.boardwidth * self.tilesize - self.selected.sprite.rect.w

    def find_upper_bound(self):
        '''Returns the upper bound for the selected tile sprite'''
        row = self.to_row(self.selected.sprite.rect.y)
        col = self.to_col(self.selected.sprite.rect.x)
        up = row - 1
        # Same basic idea as before, since a horizonal rectangle takes
        # up 2 spots in the grid, the boundaries of both spots have to be
        # checked, the first obstruction found is returned
        if self.selected.sprite.rect.w > self.selected.sprite.rect.h:
            while up >= 0:
                if not  self.empty(self.layout[up][col]):
                    return self.ymargin + up * self.tilesize + self.tilesize
                if not  self.empty(self.layout[up][col + 1]):
                    return self.ymargin + up * self.tilesize + self.tilesize
                up -= 1
            return self.ymargin    
        else: # Square or vertical rectangle   
            while up >= 0:
                if not self.empty(self.layout[up][col]):
                    return self.ymargin + up * self.tilesize + self.tilesize
                up -= 1
            return self.ymargin

    def find_lower_bound(self):
        '''Returns the lower bound for the selected tile sprite'''
        row = self.to_row(self.selected.sprite.rect.y)
        col = self.to_col(self.selected.sprite.rect.x)
        down = row + 1
        if self.selected.sprite.rect.w > self.selected.sprite.rect.h:
            while down < self.boardheight:
                if not self.empty(self.layout[down][col]):
                    return self.ymargin + down * self.tilesize - self.tilesize
                if not self.empty(self.layout[down][col + 1]):
                    return self.ymargin + down * self.tilesize - self.tilesize
                down += 1
            return self.ymargin + self.boardheight * self.tilesize - self.tilesize    
        else:    
            while down < self.boardheight:
                if not  self.empty(self.layout[down][col]):
                    return self.ymargin + down * self.tilesize - self.selected.sprite.rect.h
                down += 1
            return self.ymargin + self.boardheight * self.tilesize - self.selected.sprite.rect.h       

    def print_grid(self):
        '''Prints the grid to the console for debugging purposes'''
        for i in range(self.boardheight):
            for j in range(self.boardwidth):
                print self.layout[i][j],' ',
            print
        print
