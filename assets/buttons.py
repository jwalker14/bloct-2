import pygame
from pygame.locals import *

white = (255, 255, 255)
lightgray = (225, 225, 225)
black = (0, 0 , 0)
windowwidth = 800
windowheight = 600
scenes = {}
scene = None

class Button(pygame.sprite.Sprite):
    def __init__(self, image1, image2):
        pygame.sprite.Sprite.__init__(self)
        self.pinkbtn = image1
        self.bluebtn = image2
        self.image = self.pinkbtn
        self.rect = self.image.get_rect()

    def go_blue(self):
        self.image = self.bluebtn

    def go_pink(self):
        self.image = self.pinkbtn
        

