#!/usr/bin/python

import pygame
from pygame.locals import *
from assets.buttons import *
from assets.board import *

white = (255, 255, 255)
lightgray = (230, 230, 250)#changed to lavender
darkgray = (150, 150, 175)
black = (0, 0 , 0)
darkblue = (131, 147, 202)
windowwidth = 800
windowheight = 600
scenes = {}
scene = None


# The scene classes represent the different states of the game,
# and there is one for each separate screen. They handle drawing and events
# for that screen, and are stored in a dictionary (scenes). When the user does
# something that changes the state of the game, e.g. presses the start button,
# the scene is changed by updating a variable (scene). This variable is used in
# the main loop to draw and handle events for the current screen.

## TODO: All scenes should have their own file. This will minimize confusing when editing code.
## However, there are currently some erros that show up when moving these ones. Has something to do with the "change_board function";

class BoardScene():
    '''Represents the board game screen. Contains the board,
    handles events pertaining to the screen and draws images.'''
    def __init__(self):
        self.load_images()
        self.layout()
        self.name = 'BoardScene'
        # S - square
        # B - barrier
        # P - player
        # F - finish
        # V - vertical rectangle
        # H - horizontal rectangle
        # E - empty
        # N - null (represents the other cell a rectangle occupies,
        # which is ignored in setup)
        self.layouts = {1: [['E', 'S', 'F'],
                            ['S', 'E', 'B'],
                            ['B', 'E', 'P']],

                        2: [['F', 'V', 'E'],
                            ['B', 'N', 'P'],
                            ['E', 'S', 'E']],

                        3: [['E', 'F', 'S'],
                            ['S', 'H', 'N'],
                            ['B', 'E', 'P']],

                        4: [['B', 'S', 'F', 'E'],
                            ['E', 'S', 'B', 'V'],
                            ['B', 'H', 'N', 'N'],
                            ['S', 'E', 'P', 'B']],

                        5: [['B', 'V', 'B', 'F'],
                            ['B', 'N', 'E', 'E'],
                            ['S', 'E', 'H', 'N'],
                            ['P', 'S', 'S', 'B']],

                        6: [['E', 'S', 'V', 'F'],
                            ['B', 'E', 'N', 'V'],
                            ['S', 'H', 'N', 'N'],
                            ['P', 'S', 'S', 'B']],

                        7: [['F', 'V', 'B', 'S', 'E'],
                            ['S', 'N', 'H', 'N', 'B'],
                            ['B', 'S', 'S', 'V', 'S'],
                            ['H', 'N', 'S', 'N', 'E'],
                            ['B', 'E', 'B', 'E', 'P']],

                        8: [['B', 'S', 'V', 'S', 'E'],
                            ['B', 'E', 'N', 'B', 'E'],
                            ['E', 'V', 'S', 'H', 'N'],
                            ['S', 'N', 'B', 'E', 'E'],
                            ['F', 'B', 'E', 'P', 'B']],

                        9: [['E', 'S', 'F', 'S', 'S'],
                            ['B', 'H', 'N', 'V', 'B'],
                            ['S', 'E', 'B', 'N', 'S'],
                            ['B', 'H', 'N', 'E', 'B'],
                            ['S', 'S', 'P', 'B', 'B']],

                        10: [['E', 'S', 'B', 'S', 'B', 'B'],
                             ['F', 'H', 'N', 'E', 'E', 'B'],
                             ['B', 'B', 'V', 'B', 'S', 'E'],
                             ['P', 'B', 'N', 'S', 'V', 'B'],
                             ['E', 'S', 'H', 'N', 'N', 'E'],
                             ['B', 'S', 'B', 'E', 'B', 'E']],

                        11: [['P', 'E', 'S', 'E', 'B', 'B'],
                             ['E', 'B', 'H', 'N', 'V', 'B'],
                             ['S', 'H', 'N', 'B', 'N', 'B'],
                             ['S', 'V', 'E', 'E', 'H', 'N'],
                             ['B', 'N', 'B', 'S', 'E', 'B'],
                             ['E', 'S', 'B', 'B', 'S', 'F']],

                        12: [['B', 'E', 'S', 'E', 'V', 'B'],
                             ['S', 'H', 'N', 'B', 'N', 'E'],
                             ['S', 'V', 'P', 'B', 'S', 'V'],
                             ['E', 'N', 'B', 'F', 'B', 'N'],
                             ['E', 'H', 'N', 'E', 'H', 'N'],
                             ['B', 'B', 'S', 'S', 'B', 'B']]
                        }

        self.counts = {1: 6, 2: 7, 3: 12, 4: 11, 5: 11, 6: 18, 7: 22, 8: 27, 9: 21, 10: 15, 11: 23, 12: 27}

        self.background = pygame.Surface((windowwidth, windowheight)).convert()
        self.background.fill(lightgray)
        self.font = pygame.font.Font(None, 32)

        self.level = 1
        self.boardsaver = BoardSaver()


    def draw(self, screen):
        screen.blit(self.background, (0, 0))
        screen.blit(self.bgborder, (0, 0))
        self.board.allsprites.draw(screen)
        screen.blit(self.lvllabel, (68, 63))
        screen.blit(self.countlabel, (514, 63))
        self.buttons.draw(screen)
        #print(value, ...)

    def handle_event(self, event):
        global scene
        pos = pygame.mouse.get_pos()
        if event.type == pygame.MOUSEBUTTONDOWN:
            self.board.select_tile()
            if self.howtobtn.rect.collidepoint(pos):
                scene = scenes["Instruction"]
                scene.reset_buttons()
                pop= pygame.mixer.Sound("pop.wav")
                pop.play()
            elif self.mainmenubtn.rect.collidepoint(pos):
                scene = scenes["Menu"]
                scene.reset_buttons()
                pop= pygame.mixer.Sound("pop.wav")
                pop.play()
            elif self.restartbtn.rect.collidepoint(pos):
                self.change_board(self.layouts[self.level], None)
                pop= pygame.mixer.Sound("pop.wav")
                pop.play()
        elif event.type == pygame.MOUSEBUTTONUP:
            if not self.board.selected.sprite == None:
                self.board.drop_tile()
                if self.board.solved():
                    self.level += 1
                    self.change_board(self.layouts[self.level], None)
                if self.board.count == 0:
                    self.change_board(self.layouts[self.level], None)
                self.update_label()
            self.boardsaver.write_board(self.board.level, self.board.layout, self.board.finishcoord, self.board.count)
        elif event.type == pygame.MOUSEMOTION:
            if not self.board.selected.sprite == None:
                self.board.move_tile()
            for button in self.buttons:
                if button.rect.collidepoint(pos):
                    button.go_blue()
                else:
                    button.go_pink()


    def update(self):
        pass

    def load_images(self):
        mainmenu = pygame.image.load("images/mainmenu.jpg").convert()
        bluemainmenu = pygame.image.load("images/bluemainmenu.jpg").convert()
        restart = pygame.image.load("images/restart.jpg").convert()
        bluerestart = pygame.image.load("images/bluerestart.jpg").convert()
        instructions = pygame.image.load("images/instructions.jpg").convert()
        blueinstructions = pygame.image.load("images/blueinstructions.jpg").convert()
        self.star = pygame.image.load("images/star.jpg").convert()

        self.bgborder = pygame.image.load("images/boardscenebg.jpg").convert()
        self.bgborder.set_colorkey(white)

        self.buttons = pygame.sprite.RenderPlain()
        self.mainmenubtn = Button(mainmenu, bluemainmenu)
        self.buttons.add(self.mainmenubtn)
        self.restartbtn = Button(restart, bluerestart)
        self.buttons.add(self.restartbtn)
        self.howtobtn = Button(instructions, blueinstructions)
        self.buttons.add(self.howtobtn)



    def layout(self):
        ymar = 500
        self.howtobtn.rect.x = 50
        self.howtobtn.rect.y = ymar
        self.restartbtn.rect.x = 300
        self.restartbtn.rect.y = ymar
        self.mainmenubtn.rect.x = 550
        self.mainmenubtn.rect.y = ymar



    def draw_border(self, surface):
        '''Draws the border onto the background'''
        color = darkblue
        borderwidth = 10
        # Top
        x = self.board.xmargin - borderwidth
        y = self.board.ymargin - borderwidth
        borderlength = borderwidth * 2 + self.board.boardwidth * self.board.tilesize
        pygame.draw.rect(surface, color, (x, y, borderlength, borderwidth), 0)
        # Bottom
        y = self.board.ymargin + self.board.boardheight * self.board.tilesize
        pygame.draw.rect(surface, color, (x, y, borderlength, borderwidth), 0)
        # Left
        y = self.board.ymargin
        borderlength = self.board.boardheight * self.board.tilesize
        pygame.draw.rect(surface, color, (x, y, borderwidth, borderlength), 0)
        # Right
        x = self.board.xmargin + self.board.boardwidth * self.board.tilesize
        pygame.draw.rect(surface, color, (x, y, borderwidth, borderlength), 0)

    def change_board(self, layout, finish):
        self.background.fill(lightgray)
        self.board = Board(layout, self.level, self.counts[self.level])
        if not finish == None:
            self.board.finishcoord = finish
        self.background.blit(self.star, self.board.finishcoord)
        self.draw_border(self.background)
        self.update_label()
        #Debuggin Purposees
        #print(self.board)

    def update_label(self):
        self.countlabel = self.font.render("moves remaining: " + str(self.board.count), 1, white)
        self.lvllabel = self.font.render("level: " + str(self.level), 1, white)

    def reset_buttons(self):
        for button in self.buttons:
            button.go_pink()



class MenuScene():
    '''Represents the main menu screen. Handles events and draws
    the images for the screen.'''
    def __init__(self):
        self.load_images()
        self.layout()
        self.quitbuttonpressed = False

    def draw(self, screen):
        self.scrollingimage.draw(screen)
        screen.blit(self.title, (windowwidth / 2 - self.title.get_width() / 2, 100))
        screen.blit(self.buttonshadow, (self.start.rect.x, self.start.rect.y))
        screen.blit(self.buttonshadow, (self.continuebtn.rect.x, self.continuebtn.rect.y))
        #screen.blit(self.buttonshadow, (self.high_score.rect.x, self.high_score.rect.y))
        screen.blit(self.buttonshadow, (self.quit.rect.x, self.quit.rect.y))
        screen.blit(self.titleshadow, (windowwidth / 2 - self.title.get_width() / 2, 100))
        self.buttons.draw(screen)


    def handle_event(self, event):
        global scene
        pos = pygame.mouse.get_pos()
        if event.type == pygame.MOUSEBUTTONDOWN:
            if self.start.rect.collidepoint(pos):
                scene = scenes["Board"]
                scene.reset_buttons()
                scene.level = 1
                scene.change_board(scene.layouts[scene.level], None)
                pop= pygame.mixer.Sound("pop.wav")
                pop.play()
            elif self.quit.rect.collidepoint(pos):
                self.quitbuttonpressed = True
                pop= pygame.mixer.Sound("pop.wav")
                pop.play()
            elif self.continuebtn.rect.collidepoint(pos):
                scene = scenes["Board"]
                scene.reset_buttons()
                levelstate = scene.boardsaver.read_board()
                scene.change_board(levelstate['saved_board'], levelstate['finish'])
                scene.level = levelstate['level']
                scene.board.count = levelstate['count']
                scene.update_label()
                pop= pygame.mixer.Sound("pop.wav")
                pop.play()
        if event.type == pygame.MOUSEMOTION:
            for button in self.buttons:
                if button.rect.collidepoint(pos):
                    button.go_blue()
                else:
                    button.go_pink()


    def load_images(self):
        title = pygame.image.load("images/blocttitle.jpg").convert()
        start = pygame.image.load("images/startbtn.jpg").convert()
        bluestart = pygame.image.load("images/bluestart.jpg").convert()
        quitbtn = pygame.image.load("images/quitbtn.jpg").convert()
        bluequit = pygame.image.load("images/bluequit.jpg").convert()
        continuebtn = pygame.image.load("images/continue.jpg").convert()
        bluecontinue = pygame.image.load("images/bluecontinue.jpg").convert()
        #high_score = pygame.image.load("images/highscores.jpg").convert()
        bgscroll = pygame.image.load("images/mainmenuscroll.jpg").convert()
        titleshadow = pygame.image.load("images/titleshadow.jpg").convert()
        buttonshadow = pygame.image.load("images/buttonshadow.jpg").convert()

        title.set_colorkey(white)
        titleshadow.set_colorkey(white)
        titleshadow.set_alpha(150)
        buttonshadow.set_colorkey(white)
        buttonshadow.set_alpha(150)

        self.title = title
        self.start = Button(start, bluestart)
        self.quit = Button(quitbtn, bluequit)
        self.continuebtn = Button(continuebtn, bluecontinue)
        #self.high_score = Button(high_score)
        self.titleshadow = titleshadow
        self.buttonshadow = buttonshadow
        self.bgscroll1 = Button(bgscroll, None)
        self.bgscroll2 = Button(bgscroll, None)

        self.buttons = pygame.sprite.RenderPlain()
        self.buttons.add(self.start)
        self.buttons.add(self.continuebtn)
        #self.buttons.add(self.high_score)
        self.buttons.add(self.quit)

        self.scrollingimage = pygame.sprite.RenderPlain()
        self.scrollingimage.add(self.bgscroll1)
        self.scrollingimage.add(self.bgscroll2)




    def update(self):
        self.bgscroll1.rect.x += -1
        self.bgscroll2.rect.x += -1
        if self.bgscroll1.rect.x <= -800:
            self.bgscroll1.rect.x = 800
        if self.bgscroll2.rect.x <= -800:
            self.bgscroll2.rect.x = 800


    def layout(self):
        '''Calculates where the title and buttons are placed'''
        self.start.rect.x = windowwidth / 2 - self.start.rect.w / 2
        self.start.rect.y = 280
        self.continuebtn.rect.x = windowwidth / 2 - self.continuebtn.rect.w / 2
        self.continuebtn.rect.y = 360
        #self.high_score.rect.x = windowwidth / 2 - self.high_score.rect.w / 2
        #self.high_score.rect.y = 400
        self.quit.rect.x = windowwidth / 2 - self.quit.rect.w / 2
        self.quit.rect.y = 440
        self.bgscroll1.rect.x = 0
        self.bgscroll1.rect.y = 0
        self.bgscroll2.rect.x = 800
        self.bgscroll2.rect.y = 0

    def reset_buttons(self):
        for button in self.buttons:
            button.go_pink()


class BoardSaver():
    def __init__(self):
        pass
    def write_board(self, level, layout, finish, count):
        # To save the board in the csv file
        with open('save_state.csv', 'w') as csvfile:
            fieldnames = ['level', 'saved_board', 'finish', 'count']
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            #write the header file (overwrites previous file)
            writer.writeheader()
            #write the saved state row (overwrites previous file)
            writer.writerow({'level': level, 'saved_board': self.layout_to_string(layout),
                             'finish': self.finish_to_string(finish), 'count': count})

    def read_board(self):
        levelstate = {}
        # To read back the values from the csv file.
        with open('save_state.csv') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                levelstate.update({'level': int(row['level'])})
                levelstate.update({'saved_board': self.string_to_layout(row['saved_board'])})
                levelstate.update({'finish': self.string_to_finish(row['finish'])})
                levelstate.update({'count': int(row['count'])})

            return levelstate

    def layout_to_string(self, layout):
        string = ''
        for row in range(len(layout)):
            for col in range(len(layout[0])):
                string += str((layout[row][col]))
            string += '|'
        return string

    def string_to_layout(self, string):
        i = 0
        layout = []
        sub = []
        while i < len(string):
            if string[i] == '|':
                layout.append(sub)
                sub = []
            else:
                sub.append(string[i])
            i += 1
        return layout

    def finish_to_string(self, finish):
        return str(finish[0]) + '|' + str(finish[1])

    def string_to_finish(self, string):
        it = string.split('|')
        return (int(it[0]), int(it[1]))


class InstructionScene():
    '''InstructionScene handles drawing and events for the pop up message dialog
    that displays the game instructions to the user via a button in BoardScene'''
    def __init__(self):
        okbtn = pygame.image.load("images/okbtn.jpg").convert()
        blueokbtn = pygame.image.load("images/blueokbtn.jpg").convert()

        #self.font = pygame.font.SysFont(None, 20)
        self.font = pygame.font.Font(None, 20)
        self.background = pygame.Surface ((400, 300)).convert()
        self.bgrect = self.background.get_rect()
        self.bgrect.centerx = windowwidth / 2
        self.bgrect.centery = (windowheight / 2) - 26
        self.background.fill(darkblue)

        self.ok = Button(okbtn, blueokbtn)
        self.ok.rect.centerx = self.bgrect.centerx
        self.ok.rect.y = 354

        self.layout()

    def layout(self):
        text = ["Solve the puzzle by getting the red block from it's",
                "starting point to it's end point indicated by the red",
                "star. Move the green and yellow blocks side-to-side or",
                "up and down to clear a path for red block. The gray",
                "blocks are stationary obstacles. Once the red block",
                "has reached it's destination, the game will proceed",
                "to the next level, each puzzle harder than the last.",
                "A counter will indicate the number of moves you have",
                "remaining. If you fail to complete the puzzle in the given",
                "number of moves you will have to start the puzzle over."]

        self.lines = []

        for string in text:
            self.lines.append(self.font.render(string, 1, white))

        x = 20
        y = 20
        for line in self.lines:
            self.background.blit(line, (x, y))
            y += 20



    def draw (self, screen):
        screen.blit (self.background, self.bgrect)
        screen.blit(self.ok.image, (self.ok.rect.x, self.ok.rect.y))



    def update(self):
        pass

    def handle_event (self, event):
        global scene
        pos = pygame.mouse.get_pos()
        if event.type == pygame.MOUSEBUTTONDOWN:
            if self.ok.rect.collidepoint(pos):
                scene = scenes["Board"]
                scene.reset_buttons()
                pop= pygame.mixer.Sound("pop.wav")
                pop.play()
        if event.type == pygame.MOUSEMOTION:
            if self.ok.rect.collidepoint(pos):
                self.ok.go_blue()
            else:
                self.ok.go_pink()

    def reset_buttons(self):
        self.ok.go_pink()










def main():
    global scenes
    global scene
    pygame.init()

    screen = pygame.display.set_mode((windowwidth, windowheight))
    pygame.display.set_caption("Bloct")
    clock = pygame.time.Clock()

    scenes.update({"Menu": MenuScene()})
    scenes.update({"Board": BoardScene()})
    scenes.update({"Instruction": InstructionScene()})
    scene = scenes["Menu"]

    pygame.display.flip()

    while 1:
        clock.tick(60)
        for event in pygame.event.get():
            if event.type == QUIT:
                return
            scene.handle_event(event)

        if scenes["Menu"].quitbuttonpressed:
            return

        scene.update()
        scene.draw(screen)

        pygame.display.flip()

if __name__ == '__main__':
    main()

pygame.quit()
