This was a project built at the University of Maine by a team of 6 people. Patrick (forgot his last name) came up with the idea of the game while patrick and I (Jason) developed the game. My portion of this project was really organization. When the game came to me I reorganized the game a bit so that it was easier to work with.

Lynn Pawlowski, Project Leader
Alexandra Ireland, Editor
Andrew Cramer, Architect
Jason Walker, Programmer
Patrick Bartley, Graphics Designer
Travis Vincent, Tester